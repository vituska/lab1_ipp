package lab2Tests;

import lab2.adapter.RandomGeneratorAdapter;
import lab2.adapter.SequenceGenerator;
import org.junit.Test;

public class AdapterTest {

    @Test
    public void testAdapter(){
        RandomGeneratorAdapter adapter = new RandomGeneratorAdapter();
        SequenceGenerator sequenceGenerator = new SequenceGenerator(adapter);

        for (int i:sequenceGenerator.generate(4)){
            System.out.print(i);
        }
    }
}
