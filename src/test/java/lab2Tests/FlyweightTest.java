package lab2Tests;

import lab2.flyweight.Context;
import lab2.flyweight.Figure;
import lab2.flyweight.FiguresFactory;
import lab2.flyweight.Picture;
import org.junit.Test;

import java.awt.*;

public class FlyweightTest {

    @Test
    public void testFlyweight(){

        Figure[] figures = {
                FiguresFactory.createCircle(10),
                FiguresFactory.createSquare(20,30),
                FiguresFactory.createCircle(30),
                FiguresFactory.createSquare(30, 60),
        };

        Picture picture = FiguresFactory.createPicture(figures);
        Context context = new Context(5, 10, Color.PINK);
        picture.draw(context);
    }
}
