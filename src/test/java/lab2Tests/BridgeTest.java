package lab2Tests;

import lab2.bridge.*;
import org.junit.Test;

public class BridgeTest {

    @Test
    public void testBridge(){
        Device device = new Radio();

        BasicRemote basicRemote = new BasicRemote(device);
        basicRemote.channelUp();
        basicRemote.power();
        device.printStatus();
    }
}
