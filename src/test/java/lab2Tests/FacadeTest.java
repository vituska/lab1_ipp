package lab2Tests;

import lab2.facade.TravelFacade;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FacadeTest {

    @Test
    public void test(){
        TravelFacade travelFacade = new TravelFacade();

        Date from = new Date(2018/10/20);
        Date to = new Date(2018/10/29);

        System.out.println(travelFacade.getFlightAndAccommodation(from, to));
    }
}
