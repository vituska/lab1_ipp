package lab2Tests;

import lab2.decorator.ChocolateDecorator;
import lab2.decorator.IceCream;
import lab2.decorator.SimpleIceCream;
import lab2.decorator.StrawberryDecorator;
import org.junit.Test;

public class DecoratorTest {

    @Test
    public void testIceCreamDecorator(){
        IceCream simpleIceCream = new SimpleIceCream();
        ChocolateDecorator chocolateDecorator = new ChocolateDecorator(simpleIceCream);
        StrawberryDecorator strawberryDecorator = new StrawberryDecorator(chocolateDecorator);
        //or
        IceCream iceCream = new StrawberryDecorator(new ChocolateDecorator(new SimpleIceCream()));

        System.out.println(strawberryDecorator.makeIceCream());
    }
}
