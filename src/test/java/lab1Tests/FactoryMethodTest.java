package lab1Tests;

import laba1.factoryMethod.AbstractWriter;
import laba1.factoryMethod.FactoryMethod;
import org.json.simple.JSONObject;
import org.junit.Test;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


public class FactoryMethodTest {

    @Test
    public void testFactoryMethodJSON(){
        FactoryMethod factoryMethod = new FactoryMethod();

        JSONObject obj = new JSONObject();
        obj.put("FirstName", "Vitalina");
        obj.put("LastName", "Petrenco");

        AbstractWriter abstractWriter = factoryMethod.getWriter(obj);
        abstractWriter.write(obj, "src\\test\\resources\\JSON.txt");
    }

    @Test
    public void testFactoryMethodDOM() throws ParserConfigurationException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        // root elements
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("company");
        doc.appendChild(rootElement);

        // staff elements
        Element staff = doc.createElement("Staff");
        rootElement.appendChild(staff);

        // set attribute to staff element
        Attr attr = doc.createAttribute("id");
        attr.setValue("1");
        staff.setAttributeNode(attr);


        FactoryMethod factoryMethod = new FactoryMethod();
        AbstractWriter abstractWriter = factoryMethod.getWriter(doc);
        abstractWriter.write(doc, "src\\test\\resources\\DOM.txt");

    }
}
