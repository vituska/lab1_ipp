package lab1Tests;

import org.junit.Test;
import laba1.prototype.Item;

public class PrototypeTest {

    @Test
    public void testPrototype() throws CloneNotSupportedException {
        Item bookOne = new Item("Harry Potter and the Philosopher's Stone", "J. K. Rowling", "Moldova");

        Item bookTwo = bookOne.clone();
        bookTwo.setName("Harry Potter and the Chamber of Secrets");

        Item bookThree = bookOne.clone();
        bookThree.setName("Harry Potter and the Prisoner of Azkaban");

        System.out.print(bookOne.toString() + bookTwo.toString() + bookThree.toString());
    }
}
