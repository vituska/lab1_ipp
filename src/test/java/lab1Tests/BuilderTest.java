package lab1Tests;

import laba1.buildPattern.Persone;
import laba1.buildPattern.PersoneBuilder;
import org.junit.Test;

public class BuilderTest {
    @Test
    public void testBuilderPattern(){
        Persone personeVitalina = new PersoneBuilder("Vitalina", "Petrenco")
                .setAge(23)
                .setAddress("Chisinau city")
                .build();
        Persone personeION = new PersoneBuilder("Ion", "Vasili")
                .setAge(33)
                .setAddress("Balti city")
                .build();
        System.out.println(personeVitalina.toString() + personeION.toString());
    }
}
