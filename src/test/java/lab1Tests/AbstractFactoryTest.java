package lab1Tests;

import laba1.abstractFactory.Army;
import laba1.abstractFactory.MarsArmyFactory;
import laba1.abstractFactory.MercuryArmyFactory;
import org.junit.Test;

public class AbstractFactoryTest {

    @Test
    public void testAbstractFactory(){
        Army army = new Army();

        MarsArmyFactory marsArmyFactory = new MarsArmyFactory();
        MercuryArmyFactory mercuryArmyFactory = new MercuryArmyFactory();

        army.createArmy(marsArmyFactory);
        army.startBattle();

        army.createArmy(mercuryArmyFactory);
        army.startBattle();
    }
}
