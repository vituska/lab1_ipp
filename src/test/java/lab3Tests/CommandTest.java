package lab3Tests;

import lab3.command.LockPhoneOperation;
import lab3.command.Phone;
import lab3.command.PhoneUser;
import lab3.command.UnlockPhoneOperation;
import org.junit.Test;

public class CommandTest {

    @Test
    public void testCommand(){
        PhoneUser phoneUser = new PhoneUser();
        Phone iPhone = new Phone("iPhone");
        Phone samsung = new Phone("Samsung");

        LockPhoneOperation lockPhoneOperation = new LockPhoneOperation(iPhone);
        UnlockPhoneOperation unlockPhoneOperation = new UnlockPhoneOperation(samsung);

        phoneUser.performOperation(lockPhoneOperation);
        phoneUser.performOperation(unlockPhoneOperation);
    }
}
