package lab3Tests;

import lab3.observer.CourseInfo;
import lab3.observer.CoursesBoardCenter;
import lab3.observer.CoursesBoardBotanica;
import org.junit.Test;

public class ObserverTest {

    @Test
    public void testObserver(){
        CourseInfo courseInfo = new CourseInfo();
        CoursesBoardBotanica coursesBoardBotanica = new CoursesBoardBotanica(courseInfo);
        CoursesBoardCenter coursesBoardCenter = new CoursesBoardCenter(courseInfo);

        courseInfo.setCourses("UDS", 17.25f, 17.36f);
        courseInfo.setCourses("UDS", 18.10f, 18.22f);
    }
}
