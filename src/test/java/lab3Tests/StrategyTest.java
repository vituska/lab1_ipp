package lab3Tests;

import lab3.strategy.GoldCard;
import lab3.strategy.Guest;
import lab3.strategy.SilverCard;
import org.junit.Test;

public class StrategyTest {

    @Test
    public void testStrategy(){

        GoldCard goldCard = new GoldCard();
        SilverCard silverCard = new SilverCard();

        Guest guest = new Guest(goldCard);

        guest.buy(10, 2);
        guest.getBill();
        guest.changeDiscountCard(silverCard);
        guest.buy(10, 2);
        guest.getBill();
    }
}
