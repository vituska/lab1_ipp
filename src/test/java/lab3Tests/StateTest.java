package lab3Tests;

import lab3.state.Printer;
import org.junit.Test;

public class StateTest {

    @Test
    public void testState(){
        Printer printer = new Printer();
        printer.writeText("Do Geese See God");
        printer.writeText("Do Geese See God");
        printer.writeText("Mr Owl Ate My Metal Worm");
        printer.writeText("Mr Owl Ate My Metal Worm");
    }
}
