package lab3Tests;

import lab3.chainResponsability.MasterCard;
import lab3.chainResponsability.Visa;
import org.junit.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ChainTest {

    @Test
    public void testChain() throws Exception {

        MasterCard masterCard = new MasterCard(100);
        Visa visa = new Visa(200);

        masterCard.setNextAccount(visa);

        masterCard.transactionMaker(150);
    }
}
