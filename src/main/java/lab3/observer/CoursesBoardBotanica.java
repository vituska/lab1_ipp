package lab3.observer;

public class CoursesBoardBotanica implements Observer {
    private String currency;
    private float purchaseRate;
    private float sellingRate;
    private CourseInfo courseInfo;

    public CoursesBoardBotanica(CourseInfo courseInfo){
        this.courseInfo = courseInfo;
        courseInfo.addObserver(this);
    }

    @Override
    public void setNewCourses(String currency, float purchaseRate, float sellingRate) {
        this.currency = currency;
        this.purchaseRate = purchaseRate;
        this.sellingRate = sellingRate;
        System.out.printf("BOTANICA\nCurrency: %s, Purchase Rate: %4f, Selling Rate: %4f\n", currency, purchaseRate, sellingRate);
    }
}
