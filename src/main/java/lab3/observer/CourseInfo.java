package lab3.observer;

import java.util.LinkedList;
import java.util.List;

public class CourseInfo implements Observable{
    private List<Observer> observers = new LinkedList<>();
    private String currency;
    private float purchaseRate;
    private float sellingRate;


    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void deleteObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void sendNitrificationToObserver() {
        for(Observer observer : observers){
            observer.setNewCourses(currency, purchaseRate, sellingRate);
        }
    }

    public void setCourses(String currency, float purchaseRate, float sellingRate){
        this.currency = currency;
        this.purchaseRate = purchaseRate;
        this.sellingRate = sellingRate;
        sendNitrificationToObserver();
    }
}
