package lab3.observer;

public interface Observer {
    public void setNewCourses(String currency, float purchaseRate, float sellingRate);
}
