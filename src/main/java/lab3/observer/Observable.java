package lab3.observer;

public interface Observable {
    public void addObserver(Observer observer);
    public void deleteObserver(Observer observer);
    public void sendNitrificationToObserver();
}
