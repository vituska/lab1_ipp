package lab3.strategy;

public class SilverCard implements AndysDiscount {
    @Override
    public double getReducedPrice(double priceBeforeDiscount) {
        return priceBeforeDiscount * 0.8;
    }
}
