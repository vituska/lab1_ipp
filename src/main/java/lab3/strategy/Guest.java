package lab3.strategy;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class Guest {

    private AndysDiscount andysDiscount;
    private List<Double> pizzas;

    public Guest(AndysDiscount andysDiscount) {
        this.andysDiscount = andysDiscount;
        this.pizzas = new ArrayList<>();
    }

    public void buy(double price, int amount){
        pizzas.add(andysDiscount.getReducedPrice(price*amount));
    }

    public void getBill(){
        System.out.println(pizzas);
    }

    public void changeDiscountCard(AndysDiscount andysDiscount){
        this.andysDiscount = andysDiscount;
    }
}
