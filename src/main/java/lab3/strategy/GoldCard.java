package lab3.strategy;

public class GoldCard implements AndysDiscount {
    @Override
    public double getReducedPrice(double priceBeforeDiscount) {
        return priceBeforeDiscount * 0.5;
    }
}
