package lab3.strategy;

public interface AndysDiscount {

    double getReducedPrice(double priceBeforeDiscount);
}
