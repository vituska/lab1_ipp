package lab3.state;

public class Printer {

    private PalindromePrinter palindromePrinter;

    public Printer(){
        setPrinter(new LeftToRightPrinter());
    }

    public void setPrinter(PalindromePrinter newPalindromePrinter){
        palindromePrinter = newPalindromePrinter;
    }

    public void writeText(String text){
        palindromePrinter.writeText(this, text);
    }
}
