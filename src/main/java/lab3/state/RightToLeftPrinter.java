package lab3.state;

public class RightToLeftPrinter implements PalindromePrinter {
    @Override
    public void writeText(Printer printer, String text) {
        text = new StringBuffer(text).reverse().toString();
        System.out.println(text);
        printer.setPrinter(new LeftToRightPrinter());
    }
}
