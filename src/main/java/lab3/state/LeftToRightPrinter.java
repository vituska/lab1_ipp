package lab3.state;

public class LeftToRightPrinter implements PalindromePrinter {
    @Override
    public void writeText(Printer printer, String text) {
        System.out.println(text);
        printer.setPrinter(new RightToLeftPrinter());
    }
}
