package lab3.state;

public interface PalindromePrinter {

    public void writeText(Printer printer, String text);
}
