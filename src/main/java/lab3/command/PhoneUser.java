package lab3.command;

public class PhoneUser {
    PhoneOperation phoneOperation;

    public void performOperation(PhoneOperation phoneOperation){
        phoneOperation.perform();
    }
}
