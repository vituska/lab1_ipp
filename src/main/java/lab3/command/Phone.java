package lab3.command;

public class Phone {

    private String phoneName;

    public Phone(String phoneName) {
        this.phoneName = phoneName;
    }

    public void unlock(){
        System.out.println("Unlock phone " + phoneName);
    }

    public void lock(){
        System.out.println("Lock phone " + phoneName);
    }

}
