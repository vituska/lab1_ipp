package lab3.command;

public class LockPhoneOperation implements PhoneOperation {
    private Phone phone;

    public LockPhoneOperation(Phone phone) {
        this.phone = phone;
    }

    @Override
    public void perform(){
        phone.lock();
    }
}
