package lab3.command;

public class UnlockPhoneOperation implements PhoneOperation {
    private Phone phone;

    public UnlockPhoneOperation(Phone phone) {
        this.phone = phone;
    }

    @Override
    public void perform() {
        phone.unlock();
    }
}
