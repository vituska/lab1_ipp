package lab3.command;

public interface PhoneOperation {
    void perform();
}
