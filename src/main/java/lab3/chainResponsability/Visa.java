package lab3.chainResponsability;

public class Visa extends Account {

    private Integer balance;

    public Integer getBalance() {
        return balance;
    }

    public Visa(Integer balance) {
        this.balance = balance;
    }
}
