package lab3.chainResponsability;

public abstract class Account {

    private Account nextAccount;
    private Integer balance;

    public Integer getBalance() {
        return balance;
    }

    public void setNextAccount(Account account){
        this.nextAccount = account;
    }

    public void transactionMaker(Integer amount) throws Exception {
        if((checkBalance(amount))){
            System.out.println(String.format("Transaction successful %s$ from %s card", amount,
                    getClass().getName().substring(25)));
        } else if (!(this.nextAccount.getBalance() == null)){
            System.out.println(String.format("Cannot make transaction %s$ from %s card, only %s$ on balance", amount,
                    getClass().getName().substring(25), getBalance()));
            this.nextAccount.transactionMaker(amount);
        }else {
            throw new Exception("NO money in any existing account for transaction");
        }
    }

    public Boolean checkBalance(Integer amount){
        return  this.getBalance() >= amount;
    }
}
