package lab3.chainResponsability;

public class MasterCard extends Account {

    private Integer balance;

    public MasterCard(Integer balance) {
        this.balance = balance;
    }

    public Integer getBalance() {
        return balance;
    }
}
