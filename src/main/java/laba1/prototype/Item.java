package laba1.prototype;

public class Item implements Cloneable {

    private String name;
    private String author;
    private String importer;

    public Item(String name, String price, String importer){
        this.name = name;
        this.author = price;
        this.importer = importer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String price) {
        this.author = price;
    }

    public String getImporter() {
        return importer;
    }

    public void setImporter(String importer) {
        this.importer = importer;
    }

    @Override
    public Item clone() throws CloneNotSupportedException{
        return (Item) super.clone();
    }

    @Override
    public String toString(){
        return String.format("Book name: %s\nBook author: %s\nBook importer: %s\n", name, author, importer);
    }
}
