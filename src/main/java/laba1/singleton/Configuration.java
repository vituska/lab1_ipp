package laba1.singleton;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class Configuration {
    private static Configuration instance = null;

    private Properties props = null;
    public Properties getProps() {
        return props;
    }

    private Configuration() throws IOException {
        props = new Properties();

        try {
            FileInputStream txt = new FileInputStream(
                    new File("src\\main\\resources\\properties.txt"));
            props.load(txt);
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


    public static Configuration getInstance() throws IOException {
        if(instance == null)
            instance = new Configuration();
            return instance;
    }
}
