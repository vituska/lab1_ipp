package laba1.buildPattern;

public class PersoneBuilder {

    private Persone persone;

    public PersoneBuilder(String firstName, String lastName) {
        this.persone = new Persone(firstName, lastName);
    }

    public PersoneBuilder setAge(int age) {
        persone.age = age;
        return this;
    }

    public PersoneBuilder setAddress(String address) {
        persone.address = address;
        return this;
    }

    public Persone build(){
        return persone;
    }
}
