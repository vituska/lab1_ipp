package laba1.buildPattern;

public class Persone {

    String firstName;
    String lastName;
    int age;
    String address;

    Persone(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public String toString(){
        return String.format("First Name: %s\n Last Name: %s\n Age: %s\n Address: %s", firstName, lastName, age, address );
    }
}
