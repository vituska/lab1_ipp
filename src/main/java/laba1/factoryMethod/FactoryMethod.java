package laba1.factoryMethod;

import org.json.simple.JSONObject;
import org.w3c.dom.Document;

public class FactoryMethod {

    public AbstractWriter getWriter(Object object){
        AbstractWriter writer = null;
        if(object instanceof JSONObject){
            writer = new JSONWriter();
        }else if(object instanceof Document){
            writer = new DOMWriter();
        }
        return writer;
    }

}
