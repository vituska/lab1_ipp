package laba1.factoryMethod;

public abstract class AbstractWriter {

    public abstract void write(Object object, String path);

}
