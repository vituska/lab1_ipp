package laba1.abstractFactory;

public class MercuryArmyFactory implements ArmyFactory {
    @Override
    public Archer createArcher() {
        return new MercuryArcher();
    }

    @Override
    public Warrior createWarrior() {
        return new MercuryWarrior();
    }
}
