package laba1.abstractFactory;

public class MercuryArcher implements Archer {
    @Override
    public void shoot() {
        System.out.println("Mercury archer shooting");
    }
}
