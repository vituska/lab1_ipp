package laba1.abstractFactory;

public class Army {
    private Archer archer;
    private Warrior warrior;

    public void createArmy(ArmyFactory armyFactory){
        archer = armyFactory.createArcher();
        warrior = armyFactory.createWarrior();
    }

    public void startBattle(){
        archer.shoot();
        warrior.hit();
    }
}
