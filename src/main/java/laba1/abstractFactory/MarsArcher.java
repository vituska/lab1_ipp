package laba1.abstractFactory;

public class MarsArcher implements Archer {
    @Override
    public void shoot() {
        System.out.println("Mars archer shooting");
    }
}
