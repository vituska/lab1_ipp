package laba1.abstractFactory;

public interface Archer {
    public abstract void shoot();
}
