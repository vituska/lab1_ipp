package laba1.abstractFactory;

public interface ArmyFactory {
    Archer createArcher();
    Warrior createWarrior();
}
