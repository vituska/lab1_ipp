package laba1.abstractFactory;

public class MarsWarrior implements Warrior {
    @Override
    public void hit() {
        System.out.println("Mars warrior hitting");
    }
}
