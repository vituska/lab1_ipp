package laba1.abstractFactory;

public class MercuryWarrior implements Warrior {
    @Override
    public void hit() {
        System.out.println("Mercury warrior hitting");
    }
}
