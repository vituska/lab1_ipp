package laba1.abstractFactory;

public class MarsArmyFactory implements ArmyFactory {
    @Override
    public Archer createArcher() {
        return new MarsArcher();
    }

    @Override
    public Warrior createWarrior() {
        return new MarsWarrior();
    }
}
