package lab2.facade;

import java.util.ArrayList;
import java.util.Date;

public class TravelFacade {
    private FlightBooker flightBooker = new FlightBooker();
    private  HotelBooker hotelBooker = new HotelBooker();

    public String getFlightAndAccommodation(Date from, Date to){
        String flights = flightBooker.getFlightDetailsFor(from, to);
        String hotels = hotelBooker.getHotelsDetailFor(from, to);
        return String.format("%s\n%s", flights, hotels);
    }
}
