package lab2.decorator;

public interface IceCream {
    public String makeIceCream();
}
