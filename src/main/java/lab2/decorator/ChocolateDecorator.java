package lab2.decorator;

public class ChocolateDecorator extends IceCreamDecorator{

    public ChocolateDecorator(IceCream iceCreamWithTopping) {
        super(iceCreamWithTopping);
    }

    public String makeIceCream(){
        return iceCreamWithTopping.makeIceCream() + addChocolate();
    }

    public String addChocolate(){
        return " + Chocolate ";
    }
}
