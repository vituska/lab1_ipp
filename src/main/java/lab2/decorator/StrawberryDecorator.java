package lab2.decorator;

public class StrawberryDecorator extends IceCreamDecorator {

    public StrawberryDecorator(IceCream iceCreamWithTopping) {
        super(iceCreamWithTopping);
    }

    public String makeIceCream(){
        return iceCreamWithTopping.makeIceCream() + addStrawberry();
    }

    public String addStrawberry(){
        return "+ Strawberry";
    }
}
