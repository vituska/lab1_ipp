package lab2.decorator;

public abstract class IceCreamDecorator implements IceCream {
    protected IceCream iceCreamWithTopping;

    public IceCreamDecorator (IceCream iceCreamWithTopping){
        this.iceCreamWithTopping = iceCreamWithTopping;
    }

    public String makeIceCream(){
        return iceCreamWithTopping.makeIceCream();
    }
}
