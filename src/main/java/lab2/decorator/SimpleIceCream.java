package lab2.decorator;

public class SimpleIceCream implements IceCream{

    @Override
    public String makeIceCream() {
        return "Plain ice cream";
    }
}
