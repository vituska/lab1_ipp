package lab2.adapter;

public interface Generator {
    int next();
}
