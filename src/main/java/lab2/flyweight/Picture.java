package lab2.flyweight;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Picture implements Figure {
    List<Figure> figures;
    public Picture(Figure[] figures){
        this.figures = new LinkedList<Figure>();
        this.figures.addAll(Arrays.asList(figures));

    }

    @Override
    public void draw(Context context) {
        for (Figure f: figures){
            f.draw(context);
        }
    }
}
