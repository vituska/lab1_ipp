package lab2.flyweight;

import java.util.HashMap;
import java.util.Map;

public abstract class FiguresFactory {

    private static Map<Integer, Circle> circleMap = new HashMap<Integer, Circle>();
    private static Map<Integer, Square> squareMap = new HashMap<Integer, Square>();

    public static Picture createPicture(Figure[] figures){
        return new Picture(figures);
    }

    public static Circle createCircle(int radius){
        if (circleMap.get(radius) == null){
            circleMap.put(radius, new Circle(radius));
        }
        return circleMap.get(radius);
    }

    public static Square createSquare(int height, int weigh){
        if(squareMap.get(height+weigh) == null){
            squareMap.put(height+weigh, new Square(height, weigh));
        }
        return squareMap.get(height+weigh);
    }

}
