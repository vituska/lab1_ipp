package lab2.flyweight;

public class Square implements Figure {
    private int height, width;

    public Square(int height, int width) {
        this.height = height;
        this.width = width;
    }

    @Override
    public void draw(Context context) {

    }
}
