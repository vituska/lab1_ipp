package lab2.flyweight;

public interface Figure {
    public void draw(Context context);
}
