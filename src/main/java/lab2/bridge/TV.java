package lab2.bridge;

public class TV implements Device {
    private boolean on = false;
    private int volume = 30;
    private int channel = 1;


    @Override
    public boolean isEnabled() {
        return on;
    }

    @Override
    public void enable() {
        on = true;
    }

    @Override
    public void disable() {
        on = false;
    }

    @Override
    public int getVolume() {
        return volume;
    }

    @Override
    public void setVolume(int percent) {
        if(volume > 80){
            this.volume = 80;
        } else if(volume < 0){
            this.volume = 0;
        } else {
            this.volume = volume;
        }
    }

    @Override
    public int getChannel() {
        return channel;
    }

    @Override
    public void setChannel(int channel) {
        this.channel = channel;
    }

    @Override
    public void printStatus() {
        System.out.println("TV");
        System.out.println("TV is " + (on ? "enabled" : "disabled"));
        System.out.println("Volume is " + volume);
        System.out.println("Channel is " + channel);
    }
}
