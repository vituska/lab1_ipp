package lab2.bridge;

public class UpgradedRemote extends BasicRemote {
    Device device;

    public UpgradedRemote(Device device){
        super.device = device;
    }

    public void mute() {
        System.out.println("Remote: mute");
        device.setVolume(0);
    }
}
